Feature: Use token

	Scenario: Use token success
		Given a user with userid: "id" and username: "daniel" who has no tokens
		When the user requests 3 tokens
		Given the user receives 3 tokens
		When the user uses a token
		Then the token is accepted


	Scenario: Use same token failure
		Given a user with userid: "id" and username: "daniel" who has no tokens
		When the user requests 3 tokens
		Given the user receives 3 tokens
		When the user uses a token
		Then the token is accepted
		When the user uses the same token
		Then the token is not accepted

	Scenario: Use fake token failure
		Given token with id "123"
		When the user uses a token
		Then the token is not accepted