Feature: Get token feature

	Scenario: get token
		Given a user with userid: "id" and username: "daniel" who has no tokens
		When the user requests 3 tokens
		Then the user receives 3 tokens
		And each token is unique

	Scenario: failure
		Given a user with userid: "id" and username: "daniel" who has no tokens
		When the user requests 6 tokens
		Then Error is returned

	Scenario: get token when more than 1 token left failure
		Given a user with userid: "id" and username: "daniel" who has no tokens
		When the user requests 3 tokens
		And the user requests 3 tokens
		Then Error is returned

	Scenario: get token when 1 token left success
		Given a user with userid: "id" and username: "daniel" who has no tokens
		When the user requests 2 tokens
		Given the user receives 2 tokens
		When the user uses a token
		And the user requests 2 tokens
		Then the user receives 3 tokens