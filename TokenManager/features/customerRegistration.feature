Feature: Customer registration

  Scenario: Customer registration when token requested success
    Given a user with userid: "id" and username: "daniel" who has no tokens
    When the user requests 3 tokens
    Then the user is registered