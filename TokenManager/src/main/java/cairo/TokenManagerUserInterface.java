package cairo;

import java.io.IOException;
import java.util.ArrayList;

/**
 * @author Daniel
 * Interface between User and TokenManager
 */
public interface TokenManagerUserInterface {
    /**
     * @author Daniel
     * @param user User
     * @param amount Int
     * @return ArrayList
     * @throws IOException
     * @throws PermissionDeniedException
     */
    ArrayList<Tok> getToken(User user, int amount) throws IOException, PermissionDeniedException;

    /**
     * @author Daniel
     * @param payRequest PayRequest
     * @throws PermissionDeniedException
     * @throws IOException
     */
    void useToken(PayRequest payRequest) throws PermissionDeniedException, IOException;
}
