package cairo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

/**
 * @author Jonas
 * Business logic for the token-service
 */

public class TokenManager implements TokenManagerUserInterface {
    TokenManagerDBInterface db;
    MQConsumer mqc;
    MQProducer mqp;

    public TokenManager(TokenManagerDBInterface db) throws IOException {
        this.db = db;
        mqc = new MQConsumer(this);
        mqp = new MQProducer();
    }

    /**
     * Generates an amount of tokens for the user
     * @param user : User object
     * @param amount : amount of tokens requested
     * @return list of tokens
     * @throws IOException
     * @throws PermissionDeniedException
     */
    public ArrayList<Tok> getToken(User user,int amount) throws IOException, PermissionDeniedException {
        if(5<amount || amount<1){throw new PermissionDeniedException("token amount not between 1 and 5");}

        if(db.numberOfTokens(user)>1){
            throw new PermissionDeniedException("user has too many tokens (more than 1)");
        }

        for (int i = 0; i < amount; i++) {
            String tokId = generateRandomId();
            String barcodeUrl = "http://02267-cairo.compute.dtu.dk:8484/barcode/"+tokId;
            Tok tok = new Tok(tokId,barcodeUrl);
            db.insertToken(user,tok);
        }
        return db.getUserTokens(user);
    }

    private String generateRandomId() {
        Random rand = new Random();
        int tmp = rand.nextInt(10000000);
        return Integer.toString(tmp);
    }

    /**
     * Validates and uses a token from the tokenid in payRequest
     * @param payRequest : PayRequest object
     * @throws PermissionDeniedException
     * @throws IOException
     */
    public void useToken(PayRequest payRequest) throws PermissionDeniedException, IOException {
        if(db.hasToken(payRequest.tokenId)){
            System.out.println("token exists");
            User usr = db.getUser(payRequest.tokenId);
            System.out.println("User getted");
            if(usr!=null){
                System.out.println("User not null");
                PayRequestCPR payRequestCPR = new PayRequestCPR();
                payRequestCPR.amount = payRequest.amount;
                payRequestCPR.customerId = usr.userId;
                payRequestCPR.merchantId = payRequest.merchantId;
                System.out.println("payRequestCPR....amount: "+payRequestCPR.amount+", customerId: "+payRequestCPR.customerId+", merchantId: "+payRequestCPR.merchantId);
                mqp.userIdProduce(payRequestCPR);
            }
            System.out.println("token removed");
            db.removeToken(payRequest.tokenId);
        }else{
            System.out.println("user is null");
            throw new PermissionDeniedException("token is not valid");
        }
    }
}
