package cairo;

import java.util.ArrayList;

/**
 * @author Daniel
 * The interface between TokenManager and DB
 */
public interface TokenManagerDBInterface {
    /**
     * @author Daniel
     * @param tokenId String
     */
    void removeToken(String tokenId);

    /**
     * @author Daniel
     * @param tokenId String
     * @return Boolean
     */
    boolean hasToken(String tokenId);

    /**
     * @author Daniel
     * @param user User
     * @param tok Tok
     */
    void insertToken(User user, Tok tok);

    /**
     * @author Daniel
     * @param user User
     * @return Boolean
     */
    boolean containsUser(User user);

    /**
     * @author Daniel
     * @param user User
     * @return Int
     */
    int numberOfTokens(User user);

    /**
     * @author Daniel
     * @param user User
     * @return ArrayList
     */
    ArrayList<Tok> getUserTokens(User user);

    /**
     * @author Daniel
     * @param tokenId String
     * @return User
     */
    User getUser(String tokenId);
}
