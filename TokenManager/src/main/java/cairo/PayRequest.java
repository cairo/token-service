package cairo;

import java.math.BigDecimal;

/**
 * @author Jonas
 * Used to parse in message queues
 * Recieved from payment service to validate a token
 */

public class PayRequest implements java.io.Serializable{

	private static final long serialVersionUID = 1L;
	
    public PayRequest(){

    }

    public String merchantId;

    public String tokenId;

    public BigDecimal amount;
}
