package cairo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Daniel
 * This class is responsible for the interaction to a database holding users with lists of tokens.
 * The purpose is to map tokens to their respective users and vise versa.
 */
public class DB implements TokenManagerDBInterface {



    private Map<User,ArrayList<Tok>> tokenMap;

    public DB(){
        System.out.println("DB init");
        tokenMap = new HashMap<>();
    }

    /**
     * @author Daniel
     * Removes token with String tokenId from the database
     * @param tokenId String Is the ID of the token
     */
    public void removeToken(String tokenId) {

        for(ArrayList<Tok> tkLst :  tokenMap.values()){
            for(Tok tk : tkLst){
                if(tk.id.equals(tokenId)){
                    tkLst.remove(tk);
                    break;
                }
            }
        }
    }

    /**
     * @author Daniel
     * Checks whether token with tokenId is in the database
     * @param tokenId String Is the ID of the token
     * @return boolean Is a boolean indicating whether the database DB holds a token with the id: tokenId
     */
    public boolean hasToken(String tokenId){

        for(List<Tok> tkLst :  tokenMap.values()){
            for(Tok tk : tkLst){
                if(tk.id.equals(tokenId)){
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @author Daniel
     * Inserts a token in the token map, with a user as a key.
     * If the user already exists, then the token is added to the existing token list for the user.
     * If it doesnt exist, a user is created and the token is put into the map under the created user in a token list.
     * @param user User
     * @param tok Tok
     */
    public void insertToken(User user,Tok tok) {

        if (tokenMap.containsKey(user)){
            ArrayList<Tok> newTokenList = tokenMap.get(user);
            newTokenList.add(tok);
            tokenMap.put(user,newTokenList);
            System.out.println("Token added to existing user");
        } else{
            ArrayList<Tok> tokenLst = new ArrayList<>();
            tokenLst.add(tok);
            tokenMap.put(user,tokenLst);
            System.out.println("User not found. Adding user to db");
        }
    }

    /**
     * @author Daniel
     * Checks if the user is present in the database and if so,
     * returns a boolean that if true means that the user is present in the database.
     * @param user User
     * @return boolean
     */
    public boolean containsUser(User user) {
        return tokenMap.containsKey(user);
    }


    /**
     * @author Daniel
     * Checks the database for the number of tokens of a given user
     * @param user User
     * @return int Number of tokens for user
     */
    public int numberOfTokens(User user) {
        if(containsUser(user)){
            return tokenMap.get(user).size();
        }
        return 0;

    }

    /**
     * @author Daniel
     * Gets the tokens of a given user
     * @param user User
     * @return ArrayList
     */
    public ArrayList<Tok> getUserTokens(User user) {
        return tokenMap.get(user);
    }


    /**
     * @author Daniel
     * Gets user for a given tokenId
     * @param tokenId String
     * @return User
     */
    public User getUser(String tokenId) {
        for (User user : tokenMap.keySet()) {
            for (Tok tok : tokenMap.get(user)){
                if(tok.id.equals(tokenId)){
                    return user;
                }
            }
        }
        return null;
    }
}
