package cairo;

import com.rabbitmq.client.*;
import org.apache.commons.lang3.SerializationUtils;

import java.io.IOException;

/**
 * @author Henning
 *
 * This class sets up the consumer end of a direct message queue,
 * which allows other microservices to call token-service, such that
 * a token can be used.
 */
public class MQConsumer {

    private static String QUEUE_NAME = "PsTm";
    private TokenManagerUserInterface tokenManager;

    public MQConsumer(TokenManagerUserInterface tmUinterface) throws IOException {
        System.out.println("CONSUME, PsTm");
        this.tokenManager = tmUinterface;


        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("02267-cairo.compute.dtu.dk");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        System.out.println("queue declared");
        Consumer consumer = new DefaultConsumer(channel) {
            
        	/**
             * A function that listens for token validation requests on a specified queue.
             * When a message is consumed and the data format is correct, calls the TokenManager for using the given token.
             * @param String consumerTag
             * @param Envelope envelope
             * @param AMQP.BasicProperties properties
             * @param byte[] body The received data, expected as a PayRequest object
             * @throws IOException
             * returns nothing
             */
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println("\nCalling handleDelivery from TokenManager consumer");
            	PayRequest payReq = SerializationUtils.deserialize(body);
                try {
                    System.out.println("About to useToken with payreq");
                    System.out.println("Data retrieved: tokenId: " + payReq.tokenId + ", MerchId: " + 
                    		payReq.merchantId + ", Amount: " + payReq.amount);
                    tokenManager.useToken(payReq);

                } catch (PermissionDeniedException e) {
                    System.out.println("Permission denied in handleDelivery(consumer)");
                    e.printStackTrace();
                }


            }
        };

        channel.basicConsume(QUEUE_NAME, true, consumer);

    }

}
