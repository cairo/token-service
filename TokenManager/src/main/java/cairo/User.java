package cairo;

import java.util.Objects;

/**
 * User object. Two constructors enables instantiation both with and without a TokenManagerUserInterface
 * @author Michael
 */

public class User {
    public String userId;
    public String userName;
    TokenManagerUserInterface tokenManager;

    public User(String userId, String userName) throws PermissionDeniedException {
        if(validate(userId,userName)) {
            this.userId = userId;
            this.userName = userName;
        } else{
            throw new PermissionDeniedException("must specify a non-empty name and id");
        }
    }

    public User(String userId, String userName, TokenManagerUserInterface tokenManager) throws PermissionDeniedException {
        if(validate(userId,userName)) {
            this.userId = userId;
            this.userName = userName;
        } else{
            throw new PermissionDeniedException("must specify a non-empty name and id");
        }
        this.tokenManager = tokenManager;
    }

    /**
     * ensures that the given parameters are not empty or null
     * @param userId
     * @param userName
     * @return false if either of the parameters are empty or null
     */
    private boolean validate(String userId, String userName){
        if(userId == "" || userName == ""){
            return false;
        }
        if(userId == null || userName == null){
            return false;
        }
        return true;
    }

    /**
     * @param o Object for override equals() functionality
     * @return true of the objects are the same.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(userId, user.userId) &&
                Objects.equals(userName, user.userName);
    }

    /**
     * @return the hash of the object
     */
    @Override
    public int hashCode() {

        return Objects.hash(userId, userName);
    }
}
