package cairo;

/**
 * @author Daniel
 * An exception that is used to throw custom messages
 */
public class PermissionDeniedException extends Exception{
    /**
     * @author Daniel
     * @param message String
     */
    public PermissionDeniedException(String message) {
        super(message);
    }
}
