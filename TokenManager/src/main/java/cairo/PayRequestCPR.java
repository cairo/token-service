package cairo;

import java.math.BigDecimal;

/**
 * @author Jonas
 * Used to parse in message queues
 * When a token has been validated and a user is found.
 * this object is sent back to payment service to start a transaction
 */

public class PayRequestCPR implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
    public PayRequestCPR(){

    }

    public String merchantId;

    public String customerId;

    public BigDecimal amount;
}
