package cairo;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.apache.commons.lang3.SerializationUtils;

import java.io.IOException;

/**
 * @author Alexander
 * Sets up a producer for sending messages to the payment service.
 * Contains functionality for fowarding a PayRequestCPR object.
 */

public class MQProducer {

    private static String QUEUE_NAME = "TmPs";

    /**
     * @author Alexander
     * Function for producing a message with a customerCPR corresponding to a valid tokenId
     * @param PayRequestCPR payRequestCPR Object for storing customerCPR, MerchantCPR and Amount for a transaction
     * @throws IOException
     * @return nothing
     */
    public void userIdProduce(PayRequestCPR payRequestCPR) throws IOException {
        System.out.println("producer reached");
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("02267-cairo.compute.dtu.dk");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME,false,false,false,null);


        byte[] data = SerializationUtils.serialize(payRequestCPR);
        System.out.println("TM producer built: CustomerId: " + payRequestCPR.customerId +
        			", MerchId : " + payRequestCPR.merchantId + ", Amount: " + payRequestCPR.amount);
        channel.basicPublish("",QUEUE_NAME,null,data);
        System.out.println("DATA SENT");
        channel.close();
        connection.close();
    }
}
