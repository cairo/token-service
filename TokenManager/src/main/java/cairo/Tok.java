package cairo;

/**
 * Token object
 * @author Michael
 */

public class Tok {
    public String barcodeUrl;
    public String id;

    public Tok(String id, String barcodeUrl) {
        this.id = id;
        this.barcodeUrl = barcodeUrl;
    }
}
