import junit.framework.TestCase;
import org.junit.Test;

import cairo.*;

import java.io.IOException;

/**
 * JUNIT tests for internal testing of the User.class
 * @author Michael
 */

public class UserTest extends TestCase{
    private String name = "Jane Doe";
    private String id = "random test id";

    @Test
    public void testCreateUserNoInterfaceSuccess(){
        try {
            User user = new User(id,name);
            assertEquals(user.userId, id);
            assertEquals(user.userName, name);
        } catch(PermissionDeniedException e){   }
    }

    @Test(expected = PermissionDeniedException.class)
    public void testCreateUserFailEmptyUsername(){
        try {
            User user = new User(id,"");
        } catch(PermissionDeniedException e){
            assertEquals(e.getMessage(),"must specify a non-empty name and id");
        }
    }

    @Test(expected = PermissionDeniedException.class)
    public void testCreateUserFailEmptyID(){
        try {
            User user = new User("",name);
        } catch(PermissionDeniedException e){
            assertEquals(e.getMessage(),"must specify a non-empty name and id");
        }
    }

    @Test(expected = PermissionDeniedException.class)
    public void testCreateUserFailNullID(){
        try {
            User user = new User(null,name);
        } catch(PermissionDeniedException e){
            assertEquals(e.getMessage(),"must specify a non-empty name and id");
        }
    }

    @Test(expected = PermissionDeniedException.class)
    public void testCreateUserFailNullName(){
        try {
            User user = new User(id,null);
        } catch(PermissionDeniedException e){
            assertEquals(e.getMessage(),"must specify a non-empty name and id");
        }
    }

    @Test
    public void testCreateUserWithInterfaceSuccess(){
        try {
            TokenManagerDBInterface db = new DB();
            TokenManagerUserInterface tm = new TokenManager(db);
            User user = new User(id, name, tm);

            assertEquals(user.userId, id);
            assertEquals(user.userName, name);
        }catch(IOException | PermissionDeniedException e){

        }
    }
}
