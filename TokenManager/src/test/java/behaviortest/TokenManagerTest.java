package behaviortest;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * @author Daniel
 * Initializes cucumber
 */
@RunWith(Cucumber.class)
@CucumberOptions(features="features", 
				 snippets=SnippetType.CAMELCASE)
public class TokenManagerTest {
}