package behaviortest;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.io.IOException;
import java.util.ArrayList;

import cairo.*;

import static org.junit.Assert.*;

/**
 * @author Daniel
 * This step file is responsible for all step definitions for the feature files of TokenManager.
 * When running cucumber, this file will catch the lines in the feature file with regex
 * and run some business logic accordingly.
 */
public class TokenManagerSteps {

	private ArrayList<Tok> tokenList;
	private Tok token;
	private boolean tokenIsValid = false;
	private TokenManagerDBInterface db = new DB();
	private TokenManagerUserInterface tokenManager = new TokenManager(db);
	private User user;
	private boolean errorThrown = false;
	private boolean tokenIsInvalid = false;

	/**
	 * @author Daniel
	 * @throws IOException Exception thrown
	 */
	public TokenManagerSteps() throws IOException {
		try{
			user = new User("notDefined","notDefined", tokenManager);
		} catch(PermissionDeniedException e){

		}
	}

	/**
	 * @author Daniel
	 * @param arg1 String
	 */
	@Given("^userid \"([^\"]*)\"$")
	public void userid(String arg1) {
		user.userId = arg1;
	}

	/**
	 * @author Daniel
	 * @param arg1 String
	 */
	@Given("^username \"([^\"]*)\"$")
	public void username(String arg1) {
		user.userName = arg1;
	}

	/**
	 * @author Daniel
	 * @throws IOException Exception thrown
	 */
	@When("^the user uses a token$")
	public void useToken() throws IOException {
		try {
			PayRequest payRequest1 = new PayRequest();
			payRequest1.tokenId = token.id;
			tokenManager.useToken(payRequest1);
			tokenIsValid = true;
		} catch (PermissionDeniedException e) {
			tokenIsInvalid = true;
		}
	}

	/**
	 * @author Daniel
	 */
	@Then("^the token is accepted$")
	public void tokenAccepted() {
		assertTrue(tokenIsValid);
		tokenIsValid = false;

	}

	/**
	 * @author Daniel
	 */
	@Then("^the token is not accepted$")
	public void tokenNOTAccepted() {
		assertTrue(tokenIsInvalid);
		tokenIsInvalid = false;
	}

	/**
	 * @author Daniel
	 */
	@Then("^each token is unique$")
	public void isTokenListElementUnique() {
		for (int i = 0; i < tokenList.size()-1; i++) {
			Tok cur = tokenList.get(i);
			for (int j = i+1; j < tokenList.size(); j++) {
				assertNotSame(cur.id, tokenList.get(j).id);
			}
		}
	}

	/**
	 * @author Daniel
	 */
	@Then("^Error is returned$")
	public void isErrorReturned() {
		assertTrue(errorThrown);
		errorThrown = false;
	}

	/**
	 * @author Daniel
	 * @param tokId String
	 */
	@Given("^token with id \"([^\"]*)\"$")
	public void tokenWithId(String tokId) {
		token = new Tok(tokId,null);
	}

	/**
	 * @author Daniel
	 */
	@Then("^the user is registered$")
	public void theUserIsRegistered() {
		assertTrue(db.containsUser(user));
	}

	/**
	 * @author Daniel
	 * @param arg0 String
	 * @param arg1 String
	 */
	@Given("^a user with userid: \"([^\"]*)\" and username: \"([^\"]*)\" who has no tokens$")
	public void useridAndUsername(String arg0, String arg1) {
		user.userId = arg0;
		user.userName = arg1;
	}

	/**
	 * @author Daniel
	 * @param arg0
	 * @throws IOException Exception thrown
	 */
	@When("^the user requests (\\d+) tokens$")
	public void theUserRequestsTokens(int arg0) throws IOException {
		try {
			tokenList = tokenManager.getToken(user,arg0);
		} catch (PermissionDeniedException e) {
			errorThrown = true;
		}
	}

	/**
	 * @author Daniel
	 * @param arg0
	 */
	@Then("^the user receives (\\d+) tokens$")
	public void theUserReceivesTokens(int arg0) {
		assertEquals(arg0,tokenList.size());
		token = tokenList.get(0);
	}

	/**
	 * @author Daniel
	 * @throws IOException Exception thrown
	 */
	@When("^the user uses the same token$")
	public void theUserUsesTheSameToken() throws IOException {
		useToken();
	}
}

