import junit.framework.TestCase;
import org.junit.Test;

import cairo.*;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.assertNotEquals;

/**
 * JUNIT tests for internal testing of the TokenManager.class
 * @author Michael
 */

public class tokenmanagerTest extends TestCase {


    @Test(expected = PermissionDeniedException.class)
    public void testPermissionDeniedExceptionWhenRequestingTooManyTokens(){
        try{
            TokenManagerDBInterface db = new DB();
            TokenManagerUserInterface tm = new TokenManager(db);
            User user = new User("test ID","Jane Doe",tm);
            tm.getToken(user,6);
        } catch (IOException | PermissionDeniedException e) {
            assertEquals(e.getMessage(),"token amount not between 1 and 5");
        }
    }

    @Test(expected = PermissionDeniedException.class)
    public void testPermissionDeniedExceptionWhenRequestingTooFewTokens(){
        try{
            TokenManagerDBInterface db = new DB();
            TokenManagerUserInterface tm = new TokenManager(db);
            User user = new User("test ID","Jane Doe",tm);
            tm.getToken(user,-1);
        } catch (IOException | PermissionDeniedException e) {
            assertEquals(e.getMessage(),"token amount not between 1 and 5");
        }
    }

    @Test(expected = PermissionDeniedException.class)
    public void testPermissionDeniedExceptionWhenUserHasTooManyTokens(){
        try{
            TokenManagerDBInterface db = new DB();
            TokenManagerUserInterface tm = new TokenManager(db);
            User user = new User("test ID","Jane Doe",tm);
            tm.getToken(user,5);
            tm.getToken(user,5);
        } catch (IOException | PermissionDeniedException e) {
            assertEquals(e.getMessage(),"user has too many tokens (more than 1)");
        }
    }

    /**
     *  Note: the 'testGetOneTokenSuccessfully'-test and all other successful
     *  'token' generation tests do not test for specific token's to be
     *  returned since the fresh tokens should not have predictable values.
     */
    @Test
    public void testGetOneTokenSuccessfully(){
        try{
            TokenManagerDBInterface db = new DB();
            TokenManagerUserInterface tm = new TokenManager(db);
            User user = new User("test ID","Jane Doe",tm);
            ArrayList<Tok> tokens = tm.getToken(user,1);
            assertTrue(tokens.size()==1);
            assertTrue(tokens.get(0)!=null);
        } catch (IOException | PermissionDeniedException e) { }
    }

    @Test
    public void testGetMultipleTokensSuccessfully(){
        int amount = 3;
        try{
            TokenManagerDBInterface db = new DB();
            TokenManagerUserInterface tm = new TokenManager(db);
            User user = new User("test ID","Jane Doe",tm);
            ArrayList<Tok> tokens = tm.getToken(user,amount);
            assertTrue(tokens.size()==amount);
            for(int i = 0; i<amount;i++) {
                Tok tok = tokens.get(0);
                tokens.remove(0);
                for (Tok remainingTokens : tokens) {
                    assertNotEquals(tok.id,remainingTokens.id);
                    assertNotEquals(tok.barcodeUrl,remainingTokens.barcodeUrl);
                }
            }
        } catch (IOException | PermissionDeniedException e) { }
    }

    @Test
    public void testUseTokenSuccess(){
        try{
            TokenManagerDBInterface db = new DB();
            TokenManagerUserInterface tm = new TokenManager(db);
            User user = new User("test ID","Jane Doe",tm);
            ArrayList<Tok> tokens = tm.getToken(user,1);
            PayRequest payRequest = new PayRequest();
            payRequest.tokenId = tokens.get(0).id;
            tm.useToken(payRequest);
            //tm.useToken(tokens.get(0).id);
        } catch (IOException | PermissionDeniedException e) {
        }
    }

    @Test(expected = PermissionDeniedException.class)
    public void testUseTokenPermissionDeniedWhenUsingDoubleConsumingAToken(){
        try{
            TokenManagerDBInterface db = new DB();
            TokenManagerUserInterface tm = new TokenManager(db);
            User user = new User("test ID","Jane Doe",tm);
            ArrayList<Tok> tokens = tm.getToken(user,1);

            PayRequest payReq = new PayRequest();
            payReq.tokenId = tokens.get(0).id;
            tm.useToken(payReq);
            tm.useToken(payReq);

        } catch (IOException | PermissionDeniedException e) {
            assertEquals(e.getMessage(),"token is not valid");
        }
    }


}
