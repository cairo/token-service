package com.TokenRestAdapter;

import java.util.ArrayList;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import cairo.PermissionDeniedException;
import cairo.Tok;
import cairo.User;

/**
 * Rest adapter endpoint specifying the reachable token manager resources via REST
 * @author Michael
 */

@Path("/tokenmanager")
public class TokenEndpoint {

    /**
     * Specifies the HTTP POST resource that returns an amount of tokens to the specified user
     * @param restuser a restUser to be parsed as a JSON object
     * @param amount the amount of tokens requested
     * @return Response
     */
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    public Response requestTokens(RestUser restuser, @QueryParam("amount") int amount) {

        try {
            System.out.println("in body found restuser object with usderid:"+restuser.userID+" and username:"+restuser.username);

           User user = new User(restuser.userID,restuser.username);
           ArrayList<Tok> result = RestApplication.tm.getToken(user, amount);
           ArrayList<RestToken> tksLst = new ArrayList<>();
            for (Tok t: result) {
                tksLst.add(new RestToken(t.id,t.barcodeUrl));
            }

           return Response.status(200).type(MediaType.APPLICATION_JSON).entity(tksLst).build();
        } catch (PermissionDeniedException e) {
        	System.out.println("PermissionDeniedException in TokenEndpoint. Message: " + e.getMessage());
        	return Response.notModified(e.getMessage()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(501).build();
        }

    }

    /**
     * A simple HTTP GET resource used for testing and verifying that the service is reachable
     * @return
     */
    @GET
    public Response testConnection(){
        return Response.ok().build();
    }

}


