package com.TokenRestAdapter;

import cairo.DB;
import cairo.TokenManager;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.io.IOException;

/**
 * rest root resource-linker
 * @author Michael
 */

@ApplicationPath("/")
public class RestApplication extends Application {

    public static TokenManager tm;

    public RestApplication() {
        super();
        try {
            System.out.println("TokenManager initialized");
            tm = new TokenManager(new DB());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
