package com.TokenRestAdapter;

/**
 * user object used for rest passing user objects via JSON
 * @author Michael
 */
public class RestUser {
    public RestUser(){

    }

    public String userID;
    public String username;
}
