package com.TokenRestAdapter;

/**
 * token object used for rest passing token objects via JSON
 * @author Michael
 */

public class RestToken {
    public RestToken(String id, String url){
        this.id = id;
        this.url = url;
    }

    public String id;
    public String url;
}
