FROM fabric8/java-alpine-openjdk8-jre
COPY TokenRestAdapter/target/TokenRestAdapter-thorntail.jar /usr/src/
WORKDIR /usr/src/
CMD java -Djava.net.preferIPv4Stack=true\
    -Djava.net.preferIPv4Addresses=true\
    -jar TokenRestAdapter-thorntail.jar